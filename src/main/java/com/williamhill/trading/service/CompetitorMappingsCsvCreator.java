package com.williamhill.trading.service;

import com.williamhill.trading.platform.api.mapping.CompetitorMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Collection;

@Service
@Slf4j
public class CompetitorMappingsCsvCreator {
    private static final String COMPETITOR_MAPPING_CSV_INPUT_FORMAT = "%s,%s,%s";

    @Value("${generated.competitor.mappings.file}")
    private String competitorMappingsCsvFile;
    @Autowired
    private CompetitorCacheService competitorCacheService;

    public void create(Collection<CompetitorMapping> competitorMappings) throws IOException {
        log.info("Number of Tipex Competitors to process : " + competitorMappings.size());
        final OutputStream os = new FileOutputStream(competitorMappingsCsvFile);

        try (
                Writer outWriter = new OutputStreamWriter(os, "UTF-8");
                BufferedWriter out = new BufferedWriter(outWriter);
        ) {
            competitorMappings.stream()
                    .map(competitorMapping -> {
                        return String.format(COMPETITOR_MAPPING_CSV_INPUT_FORMAT, competitorMapping.getWhCompetitor().getId(), competitorMapping.getFeedCompetitor().getId(), competitorMapping.getFeedProvider());
                    })
                    .forEach(line -> {
                        try {
                            out.write((String) line);
                            out.write("\n");
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
            log.info("Deleted {} competitors", competitorCacheService.getDeletedRecords());
            log.info("Deleted Wrong {} competitors", competitorCacheService.getDeletedWrongRecords());
            log.info("Finished processing of {} competitors", competitorCacheService.getSavedRecords());
        }
    }
}
