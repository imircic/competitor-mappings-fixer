package com.williamhill.trading.service;

import com.williamhill.trading.platform.api.wh.WhCompetitor;
import com.williamhill.trading.util.CSVUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

@Service
@Slf4j
public class CompetitorsReader {
    @Value("${competitors.file.path}")
    private String competitorsFilePath;

    public Collection<WhCompetitor> getAll() throws Exception {
        Scanner scanner = new Scanner(new File(competitorsFilePath));
        Collection<WhCompetitor> competitors = new ArrayList<>();
        while (scanner.hasNext()) {
            List<String> line = CSVUtils.parseLine(scanner.nextLine());
            WhCompetitor competitor = new WhCompetitor();
            competitor.setId(line.get(0));
            competitor.setName(line.get(1));
            competitor.setLocation(line.get(3));
            //rest not important

            competitors.add(competitor);
        }
        scanner.close();

        return competitors;
    }
}
