package com.williamhill.trading.service;

import com.williamhill.trading.platform.api.wh.WhCompetitor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class CompetitorCacheService {
    private Map<String, WhCompetitor> whCompetitorMap = new HashMap<>();

    private AtomicInteger deletedRecords = new AtomicInteger(0);
    private AtomicInteger deletedWrongRecords = new AtomicInteger(0);
    private AtomicInteger savedRecords = new AtomicInteger(0);

    public void putAll(Collection<WhCompetitor> whCompetitors) {
        whCompetitors.forEach(whCompetitor -> {
            whCompetitorMap.put(whCompetitor.getId(), whCompetitor);
        });
    }

    public WhCompetitor get(String competitorId) {
        return whCompetitorMap.get(competitorId);
    }

    public AtomicInteger getDeletedRecords() {
        return deletedRecords;
    }

    public void setDeletedRecords(AtomicInteger deletedRecords) {
        this.deletedRecords = deletedRecords;
    }

    public AtomicInteger getSavedRecords() {
        return savedRecords;
    }

    public void setSavedRecords(AtomicInteger savedRecords) {
        this.savedRecords = savedRecords;
    }

    public AtomicInteger getDeletedWrongRecords() {
        return deletedWrongRecords;
    }

    public void setDeletedWrongRecords(AtomicInteger deletedWrongRecords) {
        this.deletedWrongRecords = deletedWrongRecords;
    }
}
