package com.williamhill.trading.service;

import com.williamhill.trading.platform.api.mapping.CompetitorMapping;
import com.williamhill.trading.platform.api.wh.WhCompetitor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CompetitorMappingsTransformer {
    private CompetitorCacheService competitorCacheService;

    public CompetitorMappingsTransformer(CompetitorCacheService competitorCacheService) {
        this.competitorCacheService = competitorCacheService;
    }

    public List<CompetitorMapping> handle(List<CompetitorMapping> competitorMappings) {

        List<CompetitorMapping> filteredList = new ArrayList<>();

        for (CompetitorMapping competitorMapping : competitorMappings) {
            log.info("competitorMapping {}", competitorMapping.getWhCompetitor().getId());

            WhCompetitor whCompetitor = competitorCacheService.get(competitorMapping.getWhCompetitor().getId());


            if (whCompetitor == null) {
                log.info("{} - all : False competitionMapping {}, will be removed", competitorCacheService.getDeletedRecords().addAndGet(1), competitorMapping.getWhCompetitor().getId());
                log.info("{} - only : False competitionMapping {}, will be removed", competitorCacheService.getDeletedWrongRecords().addAndGet(1), competitorMapping.getWhCompetitor().getId());

                continue;
            }

            log.info("WhCompetitor {}", whCompetitor.getId());

            if (whCompetitor.getName().toUpperCase().matches("^.*\\b(WOMEN|FEMENINA|FEMININO|LADIES|FEMINAS|FEMININE|FRAUEN|FEMMINILE|FEMINA|GIRLS|FEMENINO|I|II|III|A|B|C|XI|LEGENDS|RESERVES|RES|RES\\.|\\(RES\\)|U16|U17|U18|U19|U20|U21|U22|U23|SCHOOLBOYS|STUDENTS|YOUTH)\\b.*$")) {
                log.info("{} - all : Problematic competitionMapping {}, will be removed", competitorCacheService.getDeletedRecords().addAndGet(1), competitorMapping.getWhCompetitor().getId());
                continue;
            }

            log.info("{} - all : Good competitionMapping {}, stay", competitorCacheService.getSavedRecords().addAndGet(1), competitorMapping.getWhCompetitor().getId());
            filteredList.add(competitorMapping);
        }

        return filteredList;
    }
}
