package com.williamhill.trading.service;

import com.williamhill.trading.platform.api.feed.FeedCompetitor;
import com.williamhill.trading.platform.api.mapping.CompetitorMapping;
import com.williamhill.trading.platform.api.wh.WhCompetitor;
import com.williamhill.trading.util.CSVUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

@Service
@Slf4j
public class CompetitorMappingsReader {
    private static final String CSV_SEPARATOR = ",";

    @Value("${competitor.mappings.file.path}")
    private String competitorMappingsFilePath;

    public Collection<CompetitorMapping> getAll() throws Exception {
        Scanner scanner = new Scanner(new File(competitorMappingsFilePath));
        Collection<CompetitorMapping> competitorMappings = new ArrayList<>();
        while (scanner.hasNext()) {
            List<String> line = CSVUtils.parseLine(scanner.nextLine());
            CompetitorMapping competitorMapping = new CompetitorMapping();
            FeedCompetitor feedCompetitor = new FeedCompetitor();
            feedCompetitor.setId(line.get(1));
            competitorMapping.setFeedCompetitor(feedCompetitor);
            WhCompetitor whCompetitor = new WhCompetitor();
            whCompetitor.setId(line.get(0));
            competitorMapping.setWhCompetitor(whCompetitor);
            competitorMapping.setFeedProvider(line.get(2));
            //rest not important

            competitorMappings.add(competitorMapping);
        }
        scanner.close();

        return competitorMappings;
    }

}
