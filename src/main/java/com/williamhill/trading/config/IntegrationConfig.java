package com.williamhill.trading.config;

import com.williamhill.trading.platform.api.wh.WhCompetitor;
import com.williamhill.trading.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.endpoint.MethodInvokingMessageSource;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

@Configuration
public class IntegrationConfig {

    @Bean
    public CompetitorsReader competitorsReader() throws Exception {
        return new CompetitorsReader();
    }

    @Bean
    public CompetitorMappingsTransformer competitorMappingsFilter(CompetitorCacheService competitorCacheService) {
        return new CompetitorMappingsTransformer(competitorCacheService);
    }

    @Bean
    public CompetitorCacheService competitorCacheService(CompetitorsReader competitorsReader) throws Exception {
        CompetitorCacheService competitorCacheService = new CompetitorCacheService();
        Collection<WhCompetitor> whCompetitors = competitorsReader.getAll();
        competitorCacheService.putAll(whCompetitors);
        return competitorCacheService;
    }


    @Bean(name = "competitorMappingsSource")
    public MessageSource<?> competitorMappingsSource(CompetitorMappingsReader competitorMappingsReader) {
        MethodInvokingMessageSource source = new MethodInvokingMessageSource();
        source.setObject(competitorMappingsReader);
        source.setMethodName("getAll");
        return source;
    }

    @Bean
    public IntegrationFlow competitorMappingsFlow(MessageSource<?> competitorMappingsSource, CompetitorMappingsTransformer competitorMappingsTransformer) {
        return IntegrationFlows.from(competitorMappingsSource, c -> c.poller(Pollers.fixedDelay(1, TimeUnit.HOURS)))
                .transform(competitorMappingsTransformer)
                .handle("competitorMappingsCsvCreator", "create")
                .get();
    }
}
